#include <jni.h>
#ifndef _Included_ex_ndkcalculator_Main
#define _Included_ex_ndkcalculator_Main
#ifdef __cplusplus
extern "C" {
#endif
jint Java_example_ndkcalculator_MainActivity_add(JNIEnv *env, jobject obj, jint value1, jint value2 ){
    return (jint)(value1 + value2);
}
jint Java_example_ndkcalculator_MainActivity_mul(JNIEnv *env, jobject obj, jint value1, jint value2 ){
    return (jint)(value1 * value2);
}
jint Java_example_ndkcalculator_MainActivity_div(JNIEnv *env, jobject obj, jint value1, jint value2 ){
    return (jint)(value1 / value2);
}
#ifdef __cplusplus
}
#endif
#endif